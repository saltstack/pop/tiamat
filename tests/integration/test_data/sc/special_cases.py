#!/usr/bin/env python3
import asyncio
import os
import platform
import pprint
import subprocess
import sys


def modded_env():
    if None and getattr(sys, "frozen", False):
        env = os.environ.copy()
        # Remove the LOAD LIBRARY_PATH for running commands
        # https://pyinstaller.readthedocs.io/en/stable/runtime-information.html
        env.pop("DYLD_LIBRARY_PATH", None)  # Darwin
        env.pop("LD_LIBRARY_PATH", None)  # Linux
        env.pop("LIBPATH", None)  # AIX
        return env


async def run():
    return {
        "asyncio.create_subprocess_exec": (
            await (
                await asyncio.create_subprocess_exec(
                    "echo",
                    "-n",
                    "async",
                    "exec",
                    stdout=subprocess.PIPE,
                    env=modded_env(),
                )
            ).communicate()
        )[0].decode(),
        "asyncio.create_subprocess_shell": (
            await (
                await asyncio.create_subprocess_shell(
                    "echo -n async shell", stdout=subprocess.PIPE, env=modded_env()
                )
            ).communicate()
        )[0].decode(),
    }


if __name__ == "__main__":
    loop = asyncio.get_event_loop()

    result = {
        "__file__": __file__,
        "os.getcwd()": os.getcwd(),
        "os.path.abspath(__file__)": os.path.abspath(__file__),
        "sys._MEIPASS": getattr(sys, "_MEIPASS", ""),
        "sys.argv[0]": sys.argv[0],
        "sys.executable": sys.executable,
        "sys.frozen": getattr(sys, "frozen", False),
        # The following are known to have issues on SLES 12
        # https://github.com/pyinstaller/pyinstaller/issues/4657
        "platform.uname()": tuple(platform.uname()),
        "platform.system()": platform.system(),
        "subprocess.Popen": subprocess.Popen(
            ["echo", "-n", "Popen"], stdout=subprocess.PIPE, env=modded_env()
        )
        .communicate()[0]
        .decode(),
    }
    result.update(loop.run_until_complete(run()))

    pprint.pprint(result)

    loop.close()
