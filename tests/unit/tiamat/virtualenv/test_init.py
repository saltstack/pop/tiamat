from unittest import mock

from dict_tools import data


def test_bin(hub, mock_hub, bname):
    mock_hub.virtualenv.init.bin = hub.virtualenv.init.bin
    mock_hub.OPT = data.NamespaceDict(tiamat=data.NamespaceDict(venv_plugin="system"))

    mock_hub.virtualenv.init.bin(bname)
    mock_hub.virtualenv.system.bin.assert_called_once_with(bname)


def test_freeze(hub, mock_hub, bname):
    mock_hub.virtualenv.init.freeze = hub.virtualenv.init.freeze
    mods = {
        "idem-grains @ file://home/tiamat/idem-grains",
        "pop==14",
        "pytest-pop==6.3",
    }
    mock_hub.tiamat.cmd.run.return_value = data.NamespaceDict(stdout="\n".join(mods))
    freeze = mock_hub.virtualenv.init.freeze(bname)
    assert freeze == mods


def test_freeze_exclude(hub, mock_hub, bname):
    mock_hub.virtualenv.init.freeze = hub.virtualenv.init.freeze
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].pybin = ""
    mock_hub.tiamat.BUILDS[bname].srcdir = ""
    mock_hub.tiamat.BUILDS[bname].timeout = 300
    mock_hub.tiamat.BUILDS[bname].exclude = {
        "pop-config",
        "idem-grains",
        "pop==13",
    }
    mods = {
        "idem-grains @ file://home/tiamat/idem-grains",
        "pop==14",
        "pytest-pop==6.3",
    }
    mock_hub.tiamat.cmd.run.return_value = data.NamespaceDict(stdout="\n".join(mods))
    freeze = mock_hub.virtualenv.init.freeze(bname)
    assert freeze == {"pytest-pop==6.3"}


def test_create(hub, mock_hub, bname):
    mock_hub.virtualenv.init.create = hub.virtualenv.init.create
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].venv_plugin = "pyenv"

    mock_hub.virtualenv.init.create(bname)
    mock_hub.virtualenv.pyenv.create.assert_called_once_with(bname)


def test_env(hub, mock_hub, bname):
    mock_hub.virtualenv.init.env = hub.virtualenv.init.env
    ret = mock_hub.virtualenv.init.env(bname)
    assert ret == ["env", "PYTHONUTF8=1", "LANG=POSIX"]


def test_scan(hub, mock_hub, bname):
    mock_hub.virtualenv.init.scan = hub.virtualenv.init.scan
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].vroot = "test_vroot"
    mock_hub.tiamat.BUILDS[bname].omit = ["d"]
    mock_hub.tiamat.BUILDS[bname].all_paths = set()

    with mock.patch("os.walk", return_value=[("a", "b", "c"), ("d", None, None)]):
        mock_hub.virtualenv.init.scan(bname)

    assert mock_hub.tiamat.BUILDS[bname].all_paths == {"a/c", "a/b"}


def test_mk_adds(hub, mock_hub, bname, fs):
    # Let's create fake filesystem entries
    fs.create_dir("/tmp/blah/site-packages/foo")
    fs.create_file("/tmp/blah/site-packages/foo/__init__.py")
    fs.create_file("/tmp/blah/site-packages/foo/bar.py")
    fs.create_file("/tmp/blah/site-packages/foo/readme.txt")
    fs.create_dir("/tmp/blah/site-packages/foo/__pycache__")
    fs.create_file("/tmp/blah/site-packages/foo/__pycache__/__init__.py")
    fs.create_file("/tmp/blah/site-packages/foo/__pycache__/bar.py")
    fs.create_dir("/tmp/blah/site-packages/foo.dist-info")
    fs.create_file("/tmp/blah/site-packages/foo.dist-info/readme.txt")

    # Configure tiamat
    mock_hub.virtualenv.init.scan = hub.virtualenv.init.scan
    mock_hub.virtualenv.init.mk_adds = hub.virtualenv.init.mk_adds

    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].vroot = "/tmp/blah/"
    mock_hub.tiamat.BUILDS[bname].omit = []
    mock_hub.tiamat.BUILDS[bname].imports = set()
    mock_hub.tiamat.BUILDS[bname].datas = set()
    mock_hub.tiamat.BUILDS[bname].all_paths = set()

    # Scan the fake fs
    mock_hub.virtualenv.init.scan(bname)

    # Run the code being tested
    mock_hub.virtualenv.init.mk_adds(bname)

    assert mock_hub.tiamat.BUILDS[bname].datas == {
        "/tmp/blah/site-packages/foo.dist-info:foo.dist-info",
        "/tmp/blah/site-packages/foo:foo",
    }
    assert mock_hub.tiamat.BUILDS[bname].imports == {"foo", "foo.bar"}


def test_setup_pip(hub, mock_hub, bname):
    mock_hub.virtualenv.init.setup_pip = hub.virtualenv.init.setup_pip
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].srcdir = "test_src_dir"
    mock_hub.tiamat.BUILDS[bname].dir = "test_dir"
    mock_hub.tiamat.BUILDS[bname].pyinstaller_version = "9.9"
    mock_hub.tiamat.BUILDS[bname].system_copy_in = False
    mock_hub.tiamat.BUILDS[bname].exclude = None
    mock_hub.tiamat.BUILDS[bname].venv_dir = "/dev/null"
    mock_hub.tiamat.BUILDS[bname].pybin = "/dev/null/python"
    mock_hub.tiamat.BUILDS[bname].timeout = 300
    mock_hub.tiamat.BUILDS[bname].pip_version = "latest"
    mock_hub.tiamat.cmd.run.return_value = mock.MagicMock(stdout="(3, 6, 4)")

    with mock.patch("os.listdir", return_value=["foo.whl"]), mock.patch(
        "builtins.open"
    ):
        mock_hub.virtualenv.init.setup_pip(bname)
        mock_hub.tiamat.cmd.run.assert_called_with(
            [
                "/dev/null/python",
                "-m",
                "pip",
                "install",
                "foo.whl",
                "PyInstaller==9.9",
            ],
            cwd="test_src_dir",
            timeout=300,
            fail_on_error=True,
        )


def test_setup_pip_dev(hub, mock_hub, bname):
    mock_hub.virtualenv.init.setup_pip = hub.virtualenv.init.setup_pip
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].srcdir = "test_src_dir"
    mock_hub.tiamat.BUILDS[bname].dir = "test_dir"
    mock_hub.tiamat.BUILDS[bname].pyinstaller_version = "dev"
    mock_hub.tiamat.BUILDS[bname].system_copy_in = False
    mock_hub.tiamat.BUILDS[bname].exclude = None
    mock_hub.tiamat.BUILDS[bname].venv_dir = "/dev/null"
    mock_hub.tiamat.BUILDS[bname].pybin = "/dev/null/python"
    mock_hub.tiamat.BUILDS[bname].timeout = 400
    mock_hub.tiamat.BUILDS[bname].pip_version = "20.2.4"
    mock_hub.tiamat.cmd.run.return_value = mock.MagicMock(stdout="(3, 6, 4)")

    with mock.patch("os.listdir", return_value=["foo.whl"]), mock.patch(
        "builtins.open"
    ):
        mock_hub.virtualenv.init.setup_pip(bname)
        mock_hub.tiamat.cmd.run.assert_called_with(
            [
                "/dev/null/python",
                "-m",
                "pip",
                "install",
                "foo.whl",
                "https://github.com/pyinstaller/pyinstaller/tarball/develop",
            ],
            cwd="test_src_dir",
            timeout=400,
            fail_on_error=True,
        )


def test_setup_pip_local(hub, mock_hub, bname):
    mock_hub.virtualenv.init.setup_pip = hub.virtualenv.init.setup_pip
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].srcdir = "test_src_dir"
    mock_hub.tiamat.BUILDS[bname].dir = "test_dir"
    mock_hub.tiamat.BUILDS[bname].pyinstaller_version = "local:/name/a/dir/here"
    mock_hub.tiamat.BUILDS[bname].system_copy_in = False
    mock_hub.tiamat.BUILDS[bname].exclude = None
    mock_hub.tiamat.BUILDS[bname].venv_dir = "/dev/null"
    mock_hub.tiamat.BUILDS[bname].pybin = "/dev/null/python"
    mock_hub.tiamat.BUILDS[bname].timeout = 500
    # the pip_version should only be vaild for third call to cmd.run (pre_reqs install)
    # forth and final call should just be regular pip
    mock_hub.tiamat.BUILDS[bname].pip_version = "20.2.4"
    mock_hub.tiamat.cmd.run.return_value = mock.MagicMock(stdout="(3, 6, 4)")

    with mock.patch("os.listdir", return_value=["foo.whl"]), mock.patch(
        "builtins.open"
    ):
        mock_hub.virtualenv.init.setup_pip(bname)
        mock_hub.tiamat.cmd.run.assert_called_with(
            [
                "/dev/null/python",
                "-m",
                "pip",
                "install",
                "foo.whl",
                "/name/a/dir/here",
            ],
            cwd="test_src_dir",
            timeout=500,
            fail_on_error=True,
        )


def test_setup_pip_version(hub, mock_hub, bname):
    mock_hub.virtualenv.init.setup_pip = hub.virtualenv.init.setup_pip
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].srcdir = "test_src_dir"
    mock_hub.tiamat.BUILDS[bname].dir = "test_dir"
    mock_hub.tiamat.BUILDS[bname].pyinstaller_version = "dev"
    mock_hub.tiamat.BUILDS[bname].system_copy_in = False
    mock_hub.tiamat.BUILDS[bname].exclude = None
    mock_hub.tiamat.BUILDS[bname].venv_dir = "/dev/null"
    mock_hub.tiamat.BUILDS[bname].pybin = "/dev/null/python"
    mock_hub.tiamat.BUILDS[bname].timeout = 400
    # the pip_version should only be vaild for third call to cmd.run (pre_reqs install)
    mock_hub.tiamat.BUILDS[bname].pip_version = "20.2.4"
    mock_hub.tiamat.cmd.run.return_value = mock.MagicMock(stdout="(3, 6, 4)")

    with mock.patch("os.listdir", return_value=["foo.whl"]), mock.patch(
        "builtins.open"
    ):
        mock_hub.virtualenv.init.setup_pip(bname)
        kall = mock_hub.tiamat.cmd.run.mock_calls[2]
        name, args, kwargs = kall
        assert args[0] == [
            "/dev/null/python",
            "-m",
            "pip",
            "install",
            "--upgrade",
            "pip==20.2.4",
            "pyinstaller-hooks-contrib",
            "setuptools>=50.3.0",
            "wheel",
            "pycparser",
        ]
        assert kwargs == {"timeout": 400, "fail_on_error": True}
