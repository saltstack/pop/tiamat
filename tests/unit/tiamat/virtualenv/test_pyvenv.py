def test_bin(hub, mock_hub, bname):
    mock_hub.virtualenv.pyvenv.bin = hub.virtualenv.pyvenv.bin
    mock_hub.virtualenv.pyvenv.bin(bname)


def test_create(hub, mock_hub, bname):
    mock_hub.virtualenv.pyvenv.create = hub.virtualenv.pyvenv.create
    mock_hub.virtualenv.pyvenv.create(bname)
