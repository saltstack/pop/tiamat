def test_env(hub, mock_hub, bname):
    mock_hub.virtualenv.venv.env = hub.virtualenv.venv.env
    mock_hub.virtualenv.venv.env(bname)


def test_bin(hub, mock_hub, bname):
    mock_hub.virtualenv.venv.bin = hub.virtualenv.venv.bin
    mock_hub.virtualenv.venv.bin(bname)


def test_create(hub, mock_hub, bname):
    mock_hub.virtualenv.venv.create = hub.virtualenv.venv.create
    mock_hub.virtualenv.venv.create(bname)
