from unittest import mock


def test_bin(hub, mock_hub, bname):
    mock_hub.virtualenv.system.bin = hub.virtualenv.system.bin
    mock_hub.virtualenv.system.bin(bname)


def test_create(hub, mock_hub, bname):
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].venv_dir = "/tmp/venv"
    mock_hub.virtualenv.system.create = hub.virtualenv.system.create
    with mock.patch("os.symlink"), mock.patch("os.makedirs") as mock_mkdrs:
        mock_hub.virtualenv.system.create(bname)
        mock_mkdrs.assert_called_once()
