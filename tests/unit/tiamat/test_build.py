from unittest import mock


def test_mk_requirements(hub, mock_hub):
    mock_hub.tool.pyinstaller.requirements.compile = (
        hub.tool.pyinstaller.requirements.compile
    )
    req_file = "test_req>=infinity"

    with mock.patch("builtins.open", mock.mock_open(read_data=req_file)) as mock_file:
        with mock.patch("os.path.exists", return_value=True):
            result = mock_hub.tool.pyinstaller.requirements.compile(
                directory="test_dir", requirements="requirements.txt"
            )
            mock_file.assert_called_with(result, "w+")


def test_builder(hub, mock_hub, bname):
    mock_hub.builder.pyinstaller.new = hub.builder.pyinstaller.new
    mock_hub.builder.pyinstaller.run = hub.builder.pyinstaller.run
    name = "test_name"
    requirements = "test_reqs"
    sys_site = True
    exclude = {"test_exclude"}
    directory = "test_dir"
    bname = "test_build_name"

    build = mock_hub.builder.pyinstaller.new(
        name=name,
        requirements=requirements,
        sys_site=sys_site,
        exclude=exclude,
        directory=directory,
    )
    mock_hub.tiamat.BUILDS[bname] = build

    mock_hub.builder.pyinstaller.run(bname)

    mock_hub.virtualenv.init.mk_adds.assert_called_once_with(bname)
    mock_hub.tool.pyinstaller.spec.mk_spec.assert_called_once_with(bname)
    mock_hub.tool.pyinstaller.runner.call.assert_called_once_with(bname)


def test_make(hub, mock_hub, bname):
    mock_hub.tiamat.make.apply = hub.tiamat.make.apply
    mock_hub.tiamat.BUILDS[bname].srcdir = "."
    conf = {"sources": [], "make": [], "src": [], "dest": []}
    mock_hub.tiamat.BUILDS[bname].build.proj = conf

    with mock.patch("shutil.copy"), mock.patch("shutil.copytree"):
        mock_hub.tiamat.make.apply(bname)
